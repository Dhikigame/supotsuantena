<!DOCTYPE html>
<html lang="en">
<meta name="description" content="スポーツ関連のまとめブログや個人ブログをカテゴリごとに検索できるアンテナサイトです。" />
    <head>
        <title>スポーツアンテナ！ - information</title>  
        <!-- Required meta tags always come first -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <!-- ページのレイアウト 読み込み --> 
        <link rel="stylesheet" type="text/css" href="css/layout.css">
        <!-- jquery 読み込み -->
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <!-- semantic.js 読み込み -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.6/semantic.min.js" integrity="sha256-HcON65l89nxNl7vaRECVPe/zNDBzhCcJYBE7O+EcruU=" crossorigin="anonymous"></script>
        <!-- semantic.css 読み込み --> 
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.3/semantic.min.css">
        <!-- スクロール関連スクリプト -->
        <script src="js/scroll.js"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <?php require_once './header/track.php'; ?>
    </head>

    <body ontouchstart="">
    <?php 
        require_once 'header/header.php'; 
        $ua = $_SERVER['HTTP_USER_AGENT'];
        //echo $ua;
        if((strpos($ua,'iPhone')!==false) || (strpos($ua,'iPod')!==false) || (strpos($ua,'Android.*Mobile')!==false) || (strpos($ua,'Windows.*Phone')!==false) || (strpos($ua,'Android')!==false)) {
            header('Location:/sp/');
            exit();
        }
    ?>
    <div class="headline">このサイトについて</div>
    <div class="headline-text">
        スポーツ関連の5ch系まとめブログや個人ブログをまとめたアンテナサイトです。<br>
        ブログは各スポーツカテゴリごとにまとめられます。また、記事の「タイトル」「subject」を解析し関連したタグが登録されます。<br>
        これにより「チーム」「リーグ」ごとで検索すれば関連した記事が一覧で見られるようになっています。<br>
    </div>
    <div class="headline-remain">機能</div>
    <div class="headline-text">
        ・1分ごとに記事が更新されます。尚、定期的にメンテナンスが入り記事が更新されない期間がございますがご了承ください。<br>
        ・タグをクリック・タップすることでタグに関連した記事が検索されます。<br>
        ・記事タイトルの下に「タイトル」「subject」を解析した結果のタグが表示されます。<br>
        ・ブログタイトルの下に登録されているタグが表示されます。<br>
        <p class='rss_list_link'><a href="./rss_list">RSS登録ブログ一覧</a></p>
    </div>
    <div class="headline-remain">記事掲載募集！</div>
    <div class="headline-text">
        当サイトはスポーツメインのブログ配信ご希望の方、RSSフィードURLを募集しております！<br>
        以下のフォームに申請していただけましたら、内容を確認させていただきメールアドレスにご連絡致します。<br><br>
    </div>


<?php

echo "<form action='information' method='post'>";
    echo "<div class='ui form'>";
        echo "<div class='field'>";
            echo "<label>メールアドレス</label>";
            echo "<input type='email' name='email' placeholder='MailAddress'>";
        echo "</div>";
        echo "<div class='field'>";
            echo "<label>RSSフィードURL</label>";
            echo "<input type='url' name='rss' placeholder='RSSfeedURL'>";
        echo "</div>";
        echo "<input type='submit' class='ui submit button' value='申請する'></div>";
    echo "</div>";
echo "</form>";

require_once "tools/mail.php";

?>
    <div class="headline-note">
    尚、以下に相当するブログに関しましてお断りさせていただいております。
    </div>
    <div class="headline-note-text">
    ・スポーツ以外の記事がメインとなっているブログ<br>
    ・暴力・犯罪・性的コンテンツを誘導するブログ また、それらに関連した記事を掲載するブログ<br>
    ・一定期間(3ヶ月以上)更新されていないブログ<br>
    ・ブログの紹介サイトまたはアンテナサイト<br>
    ・短期間連続して申請する迷惑行為・当サイトに負荷をかける行為<br>
    ・上記以外に管理者が掲載に相応しくないと判断したブログ<br>
    </div>        

    <div class="headline-remain">URL</div>
    <div class="headline-text">
        当サイトはリンクフリーとなっております。よろしければ、ご利用のほどお願い致します。<br>
    </div>
    <div class="headline-note-text">
        【URL】<br>
        ・トップ<br>
        http://sport-antena.com/<br>
        ・野球<br>
        http://sport-antena.com/baseball<br>
        ・サッカー<br>
        http://sport-antena.com/soccer<br>
        ・バスケ<br>
        http://sport-antena.com/basketball<br><br>
        【RSS】<br>
        ・トップ<br>
        http://sport-antena.com/rss/rss2.xml<br>
        ・野球<br>
        http://sport-antena.com/rss/rss2_baseball.xml<br>
        ・サッカー<br>
        http://sport-antena.com/rss/rss2_soccer.xml<br>
        ・バスケ<br>
        http://sport-antena.com/rss/rss2_basketball.xml<br>
    </div>


    </body>
</html>