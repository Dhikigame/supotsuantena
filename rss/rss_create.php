<?php

function rss_create($feed, $article_array, $article_count, $link_title, $basename){


    // チャンネル情報の登録
    $feed->setTitle("スポーツアンテナ！");//チャンネル名
    $feed->setLink("http://sport-antena.com/");//URLアドレス
    $feed->setDescription( "スポーツのまとめブログや個人ブログをタグごとに検索できるアンテナサイトです。" );//チャンネル紹介テキスト

    // インスタンスの作成
    $item = $feed->createNewItem();

    // アイテムの情報
    $item->setTitle($link_title);//タイトル
    $item->setLink($article_array[$article_count][1]);//リンク
    $item->setDate(strtotime($article_array[$article_count][3]));//更新日時

    // アイテムの追加
    $feed->addItem($item);

    // コードの生成
    $xml = $feed->generateFeed();

    // ファイルの保存場所を設定
    if(trim($basename) == "index"){
        $file = "./rss/rss2.xml";
    }else{
        $file = "./rss/rss2_".trim($basename).".xml";
    }

    // ファイルの保存を実行
    @file_put_contents($file, $xml);
}
?>