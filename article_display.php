<?php

require_once './tag/tag_link.php';//タグのリンク付を行う
require_once './tools/img_tools.php';//
require_once './tools/url_tools.php';//
require_once './tools/text_tools.php';


/* RSS出力 */
require_once "./FeedWriter-master/Item.php";
require_once "./FeedWriter-master/Feed.php";
require_once "./FeedWriter-master/ATOM.php";
require_once "./FeedWriter-master/RSS1.php";
require_once "./FeedWriter-master/RSS2.php";
        
require_once "./rss/rss_create.php";
use \FeedWriter\RSS2;	// エイリアスの作成
$feed = new RSS2;		// インスタンスの作成

//SQLの結果を配列ごとに最大30件フェッチ
$i = 0;
while($tbl = mysqli_fetch_array($result_article)) {

    for($j = 0; $j <= 18; $j++){
        $article_array[$i][$j] = $tbl[$j];
    }
    //記事のタイトルがあるか判定し、あればエスケープ処理しループ続ける
    if($article_array[$i][0] == null){
        break;
    }else{
      $article_array[$i][0] = title_escape($article_array[$i][0]);  
    }
    $i++;

    if($i == 31){
        break;
    }
}

//print_r($article_array);
$article_all = $i;//カラムをフェッチした総数
$article_count = 0;//カラムを出力した回数
echo "<tr>";
    //表にフェッチした総数だけ記事を表示
    while($article_count < $article_all - 1) {
         $link_title = str_replace("\r\n", '', $article_array[$article_count][0]);//記事タイトル
         $image = img_judge($article_array[$article_count][2]);//画像が出力できるか判定　画像ない場合は「No Image」出力

        echo "<td>";
           echo "<center><a href=".$article_array[$article_count][1]." target=_blank><img src=".$image." ".img_size($image)." class='img-article' border='0'></a></center><br>";//画像
           echo "<a class='rss-title' href=".$article_array[$article_count][1]." target=_blank><p>".$link_title."</p></a><br>";//タイトルリンク
           $tag_count = 7;//DBに登録されているタグを出力
           while($article_array[$article_count][$tag_count] != "None"){
              echo "<a href=".tag_link($article_array[$article_count][$tag_count])."><button class='small ui basic button'>".$article_array[$article_count][$tag_count]."</button></a>";
              if($tag_count >= 18){
                break;
              }
              $tag_count++;
           }
           
           echo "<br>".$article_array[$article_count][3]."<br>";//日付
           echo "<a href=./blog?url=".$article_array[$article_count][4].">".mb_wordwrap($article_array[$article_count][5], 60, "<br>")."</a><br>";//メインサイト
           $tag_count = 0;
           $title_tags = category_tag_select($article_array[$article_count][4], $link);
          while($tag_count < 2 && $title_tags[$tag_count] != "None"){
            $title_tag_value = mb_convert_encoding($title_tags[$tag_count], "UTF-8");//UTF-8にエンコード(文字化け防止)
            echo "<a href=".tag_link($title_tag_value)."><button class='tiny ui basic button'>".$title_tags[$tag_count]."</button></a>";
            $tag_count++;
          }

         echo "</td>";
        rss_create($feed, $article_array, $article_count, $link_title, $basename);

        $article_count++;
      if($article_count % 3 == 0){
        echo "</tr>";
        echo "<tr>";
      }

    }

echo "</tr>";
?>