<?php

function tag_link($word){

    /*****

		野球

	*****/
	/****
		プロ野球
	****/
	/***	総合		***/
		if($word == "野球"){
            $tag = "野球";
            $tag_link = "./baseball?tag=".$tag;
		}
		if($word == "NPB" || $word == "プロ野球" || $word == "12球団" || $word == "オールスター" || $word == "ファン"){
            $tag = "プロ野球";
            $tag_link = "./baseball?tag=".$tag;
		}
		if($word == "高校野球" || $word == "甲子園"){
            $tag = "高校野球";
            $tag_link = "./baseball?tag=".$tag;
        	}
		if($word == "MLB" || $word == "メジャーリーグ" ){
            $tag = "MLB";
            $tag_link = "./baseball?tag=".$tag;
		}
		if($word == "セ" || $word == "セ・リーグ" || $word == "セリーグ" ){
            $tag = "セ・リーグ";
            $tag_link = "./baseball?tag=".$tag;
        	}
		if($word == "パ" || $word == "パ・リーグ" || $word == "パリーグ" ){
            $tag = "パ・リーグ";
            $tag_link = "./baseball?tag=".$tag;
		}

		/***	巨人		***/
		if($word == "巨人" || $word == "読売" || $word == "読売ジャイアンツ" || $word == "G" ){
            $tag = "巨人";
            $tag_link = "./baseball?tag=".$tag;
		}
		/***	中日		***/
		if($word == "中日" || $word == "ドラゴンズ" || $word == "中日ドラゴンズ" || $word == "D" ){
            $tag = "中日";
            $tag_link = "./baseball?tag=".$tag;
		}
		/***	ヤクルト		***/
		if($word == "ヤクルト" || $word == "スワローズ" || $word == "ヤクルトスワローズ" || $word == "東京ヤクルトスワローズ" || $word == "東京ヤクルト" || $word == "S" ){
            $tag = "ヤクルト";
            $tag_link = "./baseball?tag=".$tag;
		}
		/***	　横浜DeNA		***/
		if($word == "DeNA" || $word == "De" || $word == "Ｄｅ" || $word == "ＤｅＮＡ" || $word == "横浜" || $word == "ベイスターズ" || $word == "横浜DeNA" || $word == "横浜DeNAベイスターズ" || $word == "横浜ベイスターズ" || $word == "DB" ){
            $tag = "DeNA";
            $tag_link = "./baseball?tag=".$tag;
		}
		/***	広島東洋		***/
		if($word == "広島" || $word == "広島東洋" || $word == "カープ" || $word == "広島カープ" || $word == "広島東洋カープ" || $word == "C" ){
            $tag = "広島東洋";
            $tag_link = "./baseball?tag=".$tag;
		}
		/***	阪神		***/
		if($word == "阪神" || $word == "タイガース" || $word == "阪神タイガース" || $word == "T" ){
            $tag = "阪神";
            $tag_link = "./baseball?tag=".$tag;
		}

		/***	ソフトバンク		***/
		if($word == "ソフトバンク" || $word == "福岡ソフトバンクホークス" || $word == "ホークス" || $word == "福岡ソフトバンク" || $word == "SB" ){
            $tag = "ソフトバンク";
            $tag_link = "./baseball?tag=".$tag;
		}
		/***	日本ハム		***/
		if($word == "日本ハム" || $word == "日ハム" || $word == "日本ハムファイターズ" || $word == "北海道日本ハム" || $word == "ファイターズ" || $word == "北海道日本ハムファイターズ" || $word == "F" ){
            $tag = "日本ハム";
            $tag_link = "./baseball?tag=".$tag;
		}
		/***	ロッテ		***/
		if($word == "ロッテ" || $word == "ロッテマリーンズ" || $word == "千葉ロッテ" || $word == "千葉ロッテマリーンズ" || $word == "マリーンズ" || $word == "M" ){
            $tag = "ロッテ";
            $tag_link = "./baseball?tag=".$tag;
		}
		/***	西武		***/
		if($word == "西武" || $word == "西武ライオンズ" || $word == "ライオンズ" || $word == "埼玉西武" || $word == "埼玉西武ライオンズ" || $word == "L" ){
            $tag = "西武";
            $tag_link = "./baseball?tag=".$tag;
		}
		/***	オリックス		***/
		if($word == "オリックス" || $word == "オリックスバファローズ" || $word == "バファローズ" || $word == "オリックス・バファローズ" || $word == "Bs" ){
            $tag = "オリックス";
            $tag_link = "./baseball?tag=".$tag;
		}
		/***	楽天		***/
		if($word == "楽天" || $word == "楽天イーグルス" || $word == "東北楽天" || $word == "東北楽天ゴールデンイーグルス" || $word == "E" ){
            $tag = "楽天";
            $tag_link = "./baseball?tag=".$tag;
		}
		
		/***	サッカー		***/
		if($word == "海外サッカー" || $word == "W杯" || $word == "ワールドカップ"){
            $tag = "海外サッカー";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "サッカー" || $word == "サッカーファン" || $word == "サッカー選手"){
            $tag = "サッカー";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "Jリーグ" || $word == "国内サッカー" || $word == "天皇杯"){
            $tag = "国内サッカー";
            $tag_link = "./soccer?tag=".$tag;
		}

?>

<?php

	/*****

		サッカー

	*****/
	/***	総合		***/

		if($word == "サッカー" || $word == "サッカーファン" || $word == "サッカー選手"){
            $tag = "サッカー";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "Jリーグ" || $word == "国内サッカー" || $word == "天皇杯"){
            $tag = "Jリーグ";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "海外サッカー" || $word == "W杯" || $word == "ワールドカップ"){
            $tag = "海外サッカー";
            $tag_link = "./soccer?tag=".$tag;
		}

	/***	Jリーグチーム		***/
		if($word == "浦和" || $word == "浦和レッズ" || $word == "浦和レッドダイヤモンズ" || $word == "レッズ" || $word == "赤い悪魔"){
            $tag = "浦和";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "FC東京"){
            $tag = "FC東京";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "横浜FM" || $word == "横浜F・マリノス" || $word == "横浜マリノス" || $word == "横浜F" || $word == "横浜M"  || $word == "横浜フリューゲルス" || $word == "マリノス" || $word == "フリューゲルス"){
            $tag = "横浜FM";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "ガンバ大阪" || $word == "G大阪" || $word == "ガンバ"){
            $tag = "G大阪";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "川崎フロンターレ" || $word == "川崎" || $word == "川崎F" || $word == "フロンターレ"){
            $tag = "川崎F";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "アルビレックス新潟" || $word == "新潟" || $word == "アルビレックス" || $word == "アルビ"){
            $tag = "新潟";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "セレッソ大阪" || $word == "C大阪" || $word == "セレッソ"){
            $tag = "C大阪";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "鹿島アントラーズ" || $word == "鹿島" || $word == "アントラーズ"){
            $tag = "鹿島";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "北海道コンサドーレ札幌" || $word == "北海道コンサドーレ" || $word == "コンサドーレ札幌" || $word == "コンサドーレ" || $word == "札幌"){
            $tag = "札幌";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "ヴィッセル神戸" || $word == "ヴィッセル" || $word == "神戸"){
            $tag = "神戸";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "ジュビロ磐田" || $word == "磐田" || $word == "ジュビロ"){
            $tag = "磐田";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "名古屋グランパスエイト" || $word == "名古屋グランパス" || $word == "グランパス" || $word == "名古屋" || $word == "グランパスエイト"){
            $tag = "名古屋";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "清水エスパルス" || $word == "清水" || $word == "エスパルス"){
            $tag = "清水";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "ベガルタ仙台" || $word == "仙台" || $word == "ベガルタ"){
            $tag = "仙台";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "サガン鳥栖" || $word == "サガン" || $word == "鳥栖" || $word == "サガントス"){
            $tag = "鳥栖";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "サンフレッチェ広島F.C" || $word == "サンフレッチェ広島" || $word == "サンフレッチェ" || $word == "広島"){
            $tag = "広島";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "松本山雅FC" || $word == "松本山雅" || $word == "松本" || $word == "山雅"){
            $tag = "松本";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "柏レイソル" || $word == "柏" || $word == "レイソル" ){
            $tag = "柏";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "大宮アルディージャ" || $word == "大宮" || $word == "アルディージャ" ){
            $tag = "大宮";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "ヴァンフォーレ甲府" || $word == "ヴァンフォーレ" || $word == "甲府" ){
            $tag = "甲府";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "ジェフユナイテッド市原・千葉" || $word == "ジェフユナイテッド市原" || $word == "ジェフユナイテッド千葉"  || $word == "千葉" || $word == "市原" || $word == "ジェフ" || $word == "ジェフユナイテッド"){
            $tag = "千葉";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "アビスパ福岡" || $word == "アビスパ" || $word == "福岡"){
            $tag = "福岡";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "ファジアーノ岡山FC" || $word == "ファジアーノ岡山" || $word == "ファジアーノ" || $word == "岡山"){
            $tag = "岡山";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "湘南ベルマーレ" || $word == "湘南" || $word == "ベルマーレ" || $word == "平塚" || $word == "ベルマーレ平塚"){
            $tag = "湘南";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "モンテディオ山形" || $word == "山形" || $word == "モンテディオ"){
            $tag = "山形";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "大分トリニータ" || $word == "大分" || $word == "トリニータ"){
            $tag = "大分";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "京都サンガF.C." || $word == "京都サンガ" || $word == "京都" || $word == "サンガ" || $word == "京都パープルサンガ" || $word == "パープルサンガ"){
            $tag = "京都";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "ロアッソ熊本" || $word == "ロアッソ" || $word == "熊本"){
            $tag = "熊本";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "FC岐阜" || $word == "岐阜"){
            $tag = "岐阜";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "東京ヴェルディ1969" || $word == "東京ヴェルディ" || $word == "ヴェルディ" || $word == "東京V" || $word == "ヴェルディ川崎"){
            $tag = "東京V";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "栃木SC" || $word == "栃木"){
            $tag = "栃木";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "横浜FC"){
            $tag = "横浜FC";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "V・ファーレン長崎" || $word == "ファーレン長崎" || $word == "長崎" || $word == "ファーレン" || $word == "V・ファーレン"){
            $tag = "長崎";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "徳島ヴォルティス" || $word == "徳島" || $word == "ヴォルティス"){
            $tag = "徳島";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "ツエーゲン金沢" || $word == "ツエーゲン" || $word == "金沢"){
            $tag = "金沢";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "水戸ホーリーホック" || $word == "ホーリーホック" || $word == "水戸"){
            $tag = "水戸";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "AC長野パルセイロ" || $word == "AC長野" || $word == "パルセイロ" || $word == "長野" || $word == "長野パルセイロ"){
            $tag = "長野";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "レノファ山口FC" || $word == "レノファ山口" || $word == "山口" || $word == "レノファ"){
            $tag = "山口";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "ザスパクサツ群馬" || $word == "ザスパクサツ" || $word == "群馬" || $word == "草津" || $word == "ザツパ草津"){
            $tag = "群馬";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "愛媛FC" || $word == "愛媛"){
            $tag = "愛媛";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "FC町田ゼルビア" || $word == "町田ゼルビア" || $word == "ゼルビア" || $word == "町田"){
            $tag = "町田";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "カマタマーレ讃岐" || $word == "カマタマーレ" || $word == "讃岐" ){
            $tag = "讃岐";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "ギラヴァンツ北九州" || $word == "ギラヴァンツ" || $word == "北九州" ){
            $tag = "北九州";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "SC相模原" || $word == "相模原"){
            $tag = "相模原";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "カターレ富山" || $word == "カターレ" || $word == "富山" ){
            $tag = "富山";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "ブラウブリッツ秋田" || $word == "ブラウブリッツ" || $word == "秋田" ){
            $tag = "秋田";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "ガイナーレ鳥取" || $word == "ガイナーレ" || $word == "鳥取" ){
            $tag = "鳥取";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "FC琉球" || $word == "琉球"){
            $tag = "琉球";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "福島ユナイテッドFC" || $word == "福島ユナイテッド" || $word == "福島"){
            $tag = "福島";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "グルージャ盛岡" || $word == "グルージャ" || $word == "盛岡"){
            $tag = "盛岡";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "藤枝MYFC" || $word == "藤枝" || $word == "MYFC"){
            $tag = "藤枝";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "横浜スポーツ&カルチャークラブ" || $word == "Y.S.C.C.横浜" || $word == "YS横浜"){
            $tag = "YS横浜";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "鹿児島ユナイテッドFC" || $word == "鹿児島ユナイテッド" || $word == "鹿児島"){
            $tag = "鹿児島";
            $tag_link = "./soccer?tag=".$tag;
		}
		if($word == "ヴァンラーレ八戸" || $word == "ヴァンラーレ" || $word == "八戸"){
			$tag = "八戸";
			$tag_link = "./soccer?tag=".$tag;
		}
		if($word == "FC今治" || $word == "今治"){
			$tag = "今治";
			$tag_link = "./soccer?tag=".$tag;
		}
		if($word == "FC今治" || $word == "今治"){
			$tag = "今治";
			$tag_link = "./soccer?tag=".$tag;
		}
		if($word == "宮崎" || $word == "テゲバ" || $word == "テゲバジャーロ" || $word == "テゲバジャーロ宮崎"){
			$tag = "宮崎";
			$tag_link = "./soccer?tag=".$tag;
		}

?>

<?php

	/*****

		バスケ

	*****/
	/***	総合		***/

	if($word == "バスケ" || $word == "バスケットボール" || $word == "バスケ選手" || $word == "バスケット"){
		$tag = "バスケ";
		$tag_link = "./basketball?tag=".$tag;
	}
	if($word == "Bリーグ" || $word == "B.LEAGUE" || $word == "ジャパン・プロフェッショナル・バスケットボールリーグ" || $word == "日本プロバスケットボール" || $word == "Ｂ．ＬＥＡＧＵＥ"){
		$tag = "Bリーグ";
		$tag_link = "./basketball?tag=".$tag;
	}
	if($word == "大学バスケ" || $word == "国内バスケ"){
		$tag = "国内バスケ";
		$tag_link = "./basketball?tag=".$tag;
	}
	if($word == "NBA"){
		$tag = "NBA";
		$tag_link = "./basketball?tag=".$tag;
	}

    return $tag_link;
}
?>