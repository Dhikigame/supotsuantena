<?php

    /* PC版タグ */
    function tag_display($tag_num){

        if($tag_num === 1){
            //野球のメインタグ
            $baseball_main_tags = array('プロ野球', '高校野球', 'MLB', 'セ・リーグ', 'パ・リーグ');
            echo "<center>";
            foreach($baseball_main_tags as $baseball_main_tag){
                echo "<a href='./baseball?tag=".$baseball_main_tag."'><button class='ui basic button'>".$baseball_main_tag."</button></a>";
            }
            echo "</center>";

            //野球のプロ野球球団タグ
            $baseball_central_team_tags = array('巨人', '阪神', '中日', '広島東洋', 'DeNA', 'ヤクルト');
            $baseball_pacific_team_tags = array('オリックス', 'ソフトバンク', '日本ハム', '西武', 'ロッテ', '楽天');
            echo "<center><p>セ・リーグ球団一覧<br>";
            foreach($baseball_central_team_tags as $baseball_central_team_tag){
                echo "<a href='./baseball?tag=".$baseball_central_team_tag."'><button class='ui small basic button'>".$baseball_central_team_tag."</button></a>";
            }
            echo "<p></center>";
            echo "<center><p>パ・リーグ球団一覧<br>";
            foreach($baseball_pacific_team_tags as $baseball_pacific_team_tag){
                echo "<a href='./baseball?tag=".$baseball_pacific_team_tag."'><button class='ui small basic button'>".$baseball_pacific_team_tag."</button></a>";
            }
            echo "<p></center>";

        }

        if($tag_num === 2){
            //サッカーのメインタグ
            $soccer_main_tags = array('Jリーグ', '海外サッカー');
            echo "<center><p>";
            foreach($soccer_main_tags as $soccer_main_tag){
                echo "<a href='./soccer?tag=".$soccer_main_tag."'><button class='ui basic button'>".$soccer_main_tag."</button></a>";
            }
            echo "<p></center>";

            //サッカーのJリーグチームタグ
            echo "<center><p>Jリーグクラブ一覧";
            $soccer_jleague_team_tags = array('札幌', '八戸', '盛岡', '仙台', '秋田', '山形', '福島', '鹿島', '水戸', '栃木', '群馬', '浦和', 
                                            '大宮', '千葉', '柏', 'FC東京', '東京V', '町田', '川崎F', '横浜FM', '横浜FC', 'YS横浜',
                                            '相模原', '湘南', '甲府', '長野', '松本', '新潟', '富山', '金沢', '清水', '磐田', 
                                            '藤枝', '沼津', '名古屋', '岐阜', '京都', 'G大阪', 'C大阪', '神戸', '鳥取', '岡山', 
                                            '広島', '山口', '徳島', '讃岐', '今治', '愛媛', '福岡', '北九州', '鳥栖', '長崎', '熊本',
                                            '大分', '宮崎', '鹿児島', '琉球');
            echo "<center><p>";
            foreach($soccer_jleague_team_tags as $soccer_jleague_team_tag){
                echo "<a href='./soccer?tag=".$soccer_jleague_team_tag."'><button class='ui small basic button'>".$soccer_jleague_team_tag."</button></a>";
            }
            echo "<p></center>";

        }

        if($tag_num === 3){
            //バスケのメインタグ
            $basketball_main_tags = array('Bリーグ', '国内バスケ', 'NBA');
            echo "<center><p>";
            foreach($basketball_main_tags as $basketball_main_tag){
                echo "<a href='./basketball?tag=".$basketball_main_tag."'><button class='ui basic button'>".$basketball_main_tag."</button></a>";
            }
            echo "<p></center>";

        }
    }


    /* スマホ版タグ */
    function tag_display_sp($tag_num){
        //<a href='./baseball?tag=".$baseball_main_tag."'></a>
        if($tag_num === 1){
            //野球のメインタグ
            $baseball_main_tags = array('プロ野球', '高校野球', 'MLB', 'セ・リーグ', 'パ・リーグ');
            echo "<center>";
            foreach($baseball_main_tags as $baseball_main_tag){
                echo "<button class='ui basic button'><a href='./baseball?tag=".$baseball_main_tag."'>".$baseball_main_tag."</a></button>";
            }
            echo "</center>";

            //野球のプロ野球球団タグ
            $baseball_central_team_tags = array('yomiuri' => '巨人', 'hanshin' => '阪神', 'chunichi' => '中日', 'hirosima_toyo' => '広島東洋', 'dena' => 'DeNA', 'yakuruto' => 'ヤクルト');
            $baseball_pacific_team_tags = array('orix' => 'オリックス', 'softbank' => 'ソフトバンク', 'nipponhamu' => '日本ハム', 'seibu' => '西武', 'lotte' => 'ロッテ', 'rakuten' => '楽天');
            echo "<center><select class='ui dropdown' onChange='location.href=value;'>";
            echo "<option value='central_league'>セ・リーグ球団一覧</option>";
            foreach($baseball_central_team_tags as $class_tag => $baseball_central_team_tag){
                echo "<option class='".$class_tag."' value='./baseball?tag=".$baseball_central_team_tag."'>".$baseball_central_team_tag."</option>";
            }
            echo "</select></center>";

            echo "<center><select class='ui dropdown' onChange='location.href=value;'>";
            echo "<option value='pacific_league'>パ・リーグ球団一覧</option>";
            foreach($baseball_pacific_team_tags as $class_tag => $baseball_pacific_team_tag){
                echo "<option class='".$class_tag."' value='./baseball?tag=".$baseball_pacific_team_tag."'>".$baseball_pacific_team_tag."</button>";
            }
            echo "</select></center>";
            //スタイル関連スクリプト
            echo "<script src='../js/style.js'></script>";

        }

        if($tag_num === 2){
            //サッカーのメインタグ
            $soccer_main_tags = array('Jリーグ', '海外サッカー');
            echo "<center>";
            foreach($soccer_main_tags as $soccer_main_tag){
                echo "<button class='ui basic button'><a href='./soccer?tag=".$soccer_main_tag."'>".$soccer_main_tag."</a></button>";
            }
            echo "</center>";

            //サッカーのJリーグチームタグ
            $soccer_jleague_team_tags = array('札幌', '八戸', '盛岡', '秋田', '山形', '福島', '鹿島', '水戸', '栃木', '群馬', '浦和', 
                                            '大宮', '千葉', '柏', 'FC東京', '東京V', '町田', '川崎F', '横浜FM', '横浜FC', 'YS横浜',
                                            '相模原', '湘南', '甲府', '長野', '松本', '新潟', '富山', '金沢', '清水', '磐田', 
                                            '藤枝', '沼津', '名古屋', '岐阜', '京都', 'G大阪', 'C大阪', '神戸', '鳥取', '岡山', 
                                            '広島', '山口', '徳島', '讃岐', '今治', '愛媛', '福岡', '北九州', '鳥栖', '長崎', '熊本',
                                            '大分', '宮崎', '鹿児島', '琉球');
            echo "<center><select class='ui dropdown' onChange='location.href=value;'>";
            echo "<option value='pacific_league'>Jリーグクラブ一覧</option>";            
            foreach($soccer_jleague_team_tags as $soccer_jleague_team_tag){
                echo "<option value='./soccer?tag=".$soccer_jleague_team_tag."'>".$soccer_jleague_team_tag."</button>";
            }
            echo "</select></center>";
            //スタイル関連スクリプト
            echo "<script src='../js/style.js'></script>";

        }

        if($tag_num === 3){
            //バスケのメインタグ
            $basketball_main_tags = array('Bリーグ', '国内バスケ', 'NBA');
            echo "<center><p>";
            foreach($basketball_main_tags as $basketball_main_tag){
                echo "<button class='ui basic button'><a href='./basketball?tag=".$basketball_main_tag."'>".$basketball_main_tag."</a></button>";
            }
            echo "<p></center>";

        }
    }

?>