<?php
print<<<eof
<header class="header-sp">
  <nav class="navbar navbar-expand-lg navbar-light bg-light navbar-fixed-top">
      <a class="navbar-brand" href=./index><img class="title-sp" src="../img/sport_antena_sp.png"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
    <div class="container">
      <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item">
            <a class="nav-link h5" href=./baseball>野球<i class="baseball ball icon"></i></a>
          </li>
          <li class="nav-item">
            <a class="nav-link h5" href=./soccer>サッカー<i class="center futbol outline icon"></i></a>
          </li>
          <li class="nav-item">
            <a class="nav-link h5" href=./basketball>バスケ<i class="basketball ball icon"></i></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href=./information><i class="info circle icon"></i>Information</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
</header>
eof;
?>