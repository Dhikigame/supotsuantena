<?php

require_once '../php_firstdown/php_firstDB.php';//DBサーバーに接続
require_once '../tools/text_tools.php';//文字列編集
require_once '../db/db_timeupdate.php';//RSS更新時間取得
require_once '../time_display.php';//RSS更新時間表示
require_once '../db/db_article_soccer.php';//DBにある野球タグ関連の更新された記事を時間が新しい順に取得
require_once '../db/db_select_category.php';//ブログにあらかじめ登録されたタグを取得する(1〜2個)
require_once '../tag/tag_display.php';//ブラウザに関連タグを表示

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>
<?php
  $tag = isset($_GET['tag']) ? $_GET['tag'] : null; //GETでIDを受け取る
  if($tag == null){
    $result_article = article_query_soccer($link);//SQLの結果を出力
    echo "スポーツアンテナ！ - サッカー";
  }else{
    require_once '../db/db_article_tag.php';
    $result_article = article_query_tag($link, $tag);//SQLの結果を出力
    echo "スポーツアンテナ！ - サッカー : ".$tag;
  }
?>
    </title>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- ページのレイアウト 読み込み --> 
    <link rel="stylesheet" type="text/css" href="../css/layout.css">
    <!-- jquery 読み込み -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <!-- semantic.js 読み込み -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.6/semantic.min.js" integrity="sha256-HcON65l89nxNl7vaRECVPe/zNDBzhCcJYBE7O+EcruU=" crossorigin="anonymous"></script>
    <!-- semantic.css 読み込み --> 
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.3/semantic.min.css">
    <!-- BootStrap.css読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- BootStrap関連スクリプト読み込み -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <!-- スクロール関連スクリプト -->
    <script src="../js/scroll.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <?php require_once '../header/track.php'; ?>
  </head>

  <body ontouchstart="">
    <?php require_once './header/header.php'; ?>
    <img src='../img/pagetop.png' id='top' width='30' height='100'>

    <!-- コンテンツ表示 -->

    <div class="time-sp" style="text-align:right;">
      <?php time_display(timeupdate($link));//更新時刻取得 ?>
    </div>
    
    <div class="pusher">
      <center>
        <h1>サッカー</h1>
      </center>
    </div>

    <div class=" ui center aligned container">
      <table class="ui celled table">
        <tbody>
<?php

//タグ検索の場合は表示
if($tag != null){
  echo "<div class='searchbox'>";
    echo "<div class='searchbox-title'>検索タグ</div>";
    echo "<p>".$tag."</p>";
  echo "</div>";
}

$tag_num = 2;//サッカー：2
tag_display_sp($tag_num);
require_once './article_display_sp.php';//ブラウザに記事一覧表示

?>

        </tbody>
      </table>
    </div>
  </body>
</html>

<?php
require_once '../php_firstdown/php_downDB.php';//DB接続を終了
?>