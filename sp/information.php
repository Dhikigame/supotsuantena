<!DOCTYPE html>
<html lang="en">
<meta name="description" content="スポーツ関連のまとめブログや個人ブログをカテゴリごとに検索できるアンテナサイトです。" />
    <head>
        <title>スポーツアンテナ！ - information</title>  
        <!-- Required meta tags always come first -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <!-- ページのレイアウト 読み込み --> 
        <link rel="stylesheet" type="text/css" href="../css/layout.css">   
        <!-- jquery 読み込み -->
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <!-- semantic.js 読み込み -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.6/semantic.min.js" integrity="sha256-HcON65l89nxNl7vaRECVPe/zNDBzhCcJYBE7O+EcruU=" crossorigin="anonymous"></script>
        <!-- semantic.css 読み込み --> 
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.3/semantic.min.css">
        <!-- BootStrap.css読み込み -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <!-- BootStrap関連スクリプト読み込み -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <!-- スクロール関連スクリプト -->
        <script src="../js/scroll.js"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <?php require_once '../header/track.php'; ?>
    </head>

    <body ontouchstart="">
    <?php require_once './header/header.php'; ?>
    <div class="headline-sp">このサイトについて</div>
    <div class="headline-text-sp">
        スポーツ関連の5ch系まとめブログや個人ブログを<br>まとめたアンテナサイトです。<br>
        ブログは各スポーツカテゴリごとにまとめられます。また、記事の「タイトル」「subject」を解析し関連したタグが登録されます。<br>
        これにより「チーム」「リーグ」ごとで検索すれば<br>関連した記事が一覧で見られるようになっています。<br>
    </div>
    <div class="headline-remain-sp">機能</div>
    <div class="headline-text-sp">
        ・1分ごとに記事が更新されます。<br>尚、定期的にメンテナンスが入り記事が更新されない期間がございますがご了承ください。<br>
        ・タグをクリック・タップすることでタグに関連した記事が検索されます。<br>
        ・記事タイトルの下に「タイトル」「subject」を<br>解析した結果のタグが表示されます。<br>
        ・ブログタイトルの下に登録されているタグが<br>表示されます。<br>
        <p class='rss_list_link'><a href="./rss_list">RSS登録ブログ一覧</a></p>
    </div>
    <div class="headline-remain-sp">記事掲載募集！</div>
    <div class="headline-text-sp">
        当サイトはスポーツメインのブログ配信ご希望の方、RSSフィードURLを募集しております！<br>
        以下のフォームに申請していただけましたら、<br>内容を確認させていただきメールアドレスに<br>ご連絡致します。<br>
    </div>


<?php

echo "<form action='information' method='post'>";
    echo "<div class='ui form'>";
        echo "<div class='field'>";
            echo "<label>メールアドレス</label>";
            echo "<input type='email' name='email' placeholder='MailAddress'>";
        echo "</div>";
        echo "<div class='field'>";
            echo "<label>RSSフィードURL</label>";
            echo "<input type='url' name='rss' placeholder='RSSfeedURL'>";
        echo "</div>";
        echo "<input type='submit' class='ui submit button' value='申請する'></div>";
    echo "</div>";
echo "</form>";

require_once "../tools/mail.php";

?>
    <div class="headline-note-sp">
    尚、以下に相当するブログに関しましてお断り<br>させていただいております。
    </div>
    <div class="headline-text-sp">
    ・スポーツ以外の記事がメインとなっているブログ<br>
    ・暴力・犯罪・性的コンテンツを誘導するブログ<br>また、それらに関連した記事を掲載するブログ<br>
    ・一定期間(3ヶ月以上)更新されていないブログ<br>
    ・ブログの紹介サイトまたはアンテナサイト<br>
    ・短期間連続して申請する迷惑行為・当サイトに負荷をかける行為<br>
    ・上記以外に管理者が掲載に相応しくないと判断したブログ<br>
    </div>        

    <div class="headline-remain-sp">URL</div>
    <div class="headline-text-sp">
        当サイトはリンクフリーとなっております。<br>よろしければ、ご利用のほどお願い致します。<br>
    </div>
    <div class="headline-text-sp">
        【URL】<br>
        ・トップ<br>
        http://sport-antena.com/<br>
        ・野球<br>
        http://sport-antena.com/baseball<br>
        ・サッカー<br>
        http://sport-antena.com/soccer<br>
        ・バスケ<br>
        http://sport-antena.com/basketball<br><br>
        【RSS】<br>
        ・トップ<br>
        http://sport-antena.com/rss/rss2.xml<br>
        ・野球<br>
        http://sport-antena.com/rss/rss2_baseball.xml<br>
        ・サッカー<br>
        http://sport-antena.com/rss/rss2_soccer.xml<br>
        ・バスケ<br>
        http://sport-antena.com/rss/rss2_basketball.xml<br>
    </div>


    </body>
</html>