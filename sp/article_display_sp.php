<?php

require_once '../tag/tag_link.php';//タグのリンク付を行う
require_once '../tools/text_tools.php';

//SQLの結果を配列ごとに最大30件フェッチ
$i = 0;
while($tbl = mysqli_fetch_array($result_article)) {

    for($j = 0; $j <= 18; $j++){
        $article_array[$i][$j] = $tbl[$j];
    }
    if($article_array[$i][0] == null){
        break;
    }else{
        $article_array[$i][0] = title_escape($article_array[$i][0]);  
    }
    $i++;

    if($i == 31){
        break;
    }
}

//print_r($article_array);
$article_all = $i;//カラムをフェッチした総数
$article_count = 0;//カラムを出力した回数
echo "<tr>";
    //表にフェッチした総数だけ記事を表示
    while($article_count < $article_all - 1) {
         //print_r($tbl);
         //$link_title = mb_convert_encoding($article_array[$article_count][0], "UTF-8");//UTF-8にエンコード(文字化け防止)
         //$link_title = str_replace([a-z]{7}, '', $article_array[$article_count][0]);//記事タイトル
         $link_title = $article_array[$article_count][0];
         //$image = img_judge($article_array[$article_count][2]);//画像が出力できるか判定　画像ない場合は「No Image」出力
      echo "<tr>";
        echo "<td>";
           //print mb_strlen( $article_array[$article_count][0] )."<br>";
           //echo "<center><a href=".$article_array[$article_count][1]." target=_blank><img src=".$image." ".img_size($image)." border='0'></a></center><br>";//画像
           echo "<a href=".$article_array[$article_count][1]." target=_blank>".$link_title."</a><br>";//タイトルリンク mb_wordwrap($link_title, 30, "<br>")
           $tag_count = 7;//DBに登録されているタグを出力
           echo "記事登録タグ　　";
           while($article_array[$article_count][$tag_count] != "None"){
              echo "<a href=".tag_link($article_array[$article_count][$tag_count]).">".$article_array[$article_count][$tag_count]."</a>";
              //if($tag_count == 11 || $tag_count == 16){
              //  echo "<br>";
              //}
              if($tag_count >= 18){
                break;
              }
              //echo $tag_count;
              $tag_count++;
           }
 
           echo "<br>";

           $tag_count = 0;
           $title_tags = category_tag_select($article_array[$article_count][4], $link);
           echo "サイト登録タグ　";
          while($tag_count < 2 && $title_tags[$tag_count] != "None"){
            $title_tag_value = mb_convert_encoding($title_tags[$tag_count], "UTF-8");//UTF-8にエンコード(文字化け防止)
            echo "<a href=".tag_link($title_tag_value).">".$title_tag_value."</a>";
            $tag_count++;
          }
           
           echo "<div class='time-sp-display' align='right'>".$article_array[$article_count][3]."</div>";//日付
           echo "<div class='main_site' align='right'><a href=./blog?url=".$article_array[$article_count][4].">".mb_wordwrap($article_array[$article_count][5], 60, "<br>")."</a></div>";//メインサイト

        echo "</td>";
        $article_count++;
      //if($article_count % 3 == 0){
      echo "</tr>";
      //}

    }

echo "</tr>";
?>