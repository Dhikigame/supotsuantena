<?php
//Mysqlへ接続
function MYSQL_connect(){
	$IPADDR = isset($_SERVER['SERVER_ADDR'])?$_SERVER['SERVER_ADDR']:gethostbyname(gethostname());

	if($IPADDR == '160.16.226.147' || $IPADDR == '192.168.0.2'){//WebサーバからDBサーバに接続
		$link = mysqli_connect('localhost','root','Komike81@1');
	}else if($IPADDR == '160.16.217.168' || $IPADDR == '192.168.0.1'){//DBサーバ自身に接続
		$link = mysqli_connect('192.168.0.2','root','Komike81@1');
	}else if(strtoupper(substr(PHP_OS, 0, 6)) === 'LINUX') {//WindowsのUbuntu接続
    	$link = mysqli_connect('localhost','root','komike81');
	}else if(strtoupper(substr(PHP_OS, 0, 6)) === 'DARWIN'){//Mac OSのMAMP接続
		$link = mysqli_connect('localhost','root','root');
	}else{
		$link = FALSE;
	}


	if(!$link){
		//die("DBに接続できませんでした".mysql_error());
		die("エラー：50001");
	}

	return $link;
}

//DB選択
function DB_select($link){
	$db_selected = mysqli_select_db($link, 'MATOME_ANTENNA');
	if (!$db_selected){
    	die("エラー：50009");
	}
	return $db_selected;
}
function DB_select_category($link){
	$db_selected = mysqli_select_db($link, 'CATEGORY_MATOME');
	if (!$db_selected){
    	die("エラー：50019");
	}
	return $db_selected;
}


//Mysql接続閉じる
function MYSQL_close($link){
	mysqli_close($link);
}
?>