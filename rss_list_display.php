<?php

//SQLの結果を配列ごとにRSS登録している数だけフェッチ
$i = 0;
while($tbl = mysqli_fetch_array($result_rss_list[0])) {

    for($j = 0; $j <= 5; $j++){
        $rss_list_array[$i][$j] = $tbl[$j];
    }
    if($rss_list_array[$i][0] == null){
        break;
    }
      $i++;

    if($i > $result_rss_list[1]){
        break;
    }
}

$rss_list_all = $i;//カラムをフェッチした総数
$rss_list_count = 0;//カラムを出力した回数

echo "<thead>";
    echo "<tr>";
        echo "<th class='four wide column'>ブログタイトル</th>";
        echo "<th class='three wide column'>ブログURL</th>";
        echo "<th class='two wide column'>カテゴリタグ1</th>";
        echo "<th class='two wide column'>カテゴリタグ2</th>";
        echo "<th class='three wide column'>RSS登録日</th>";
        echo "<th class='three wide column'>最新記事更新日</th>";
    echo "</tr>";
echo "</thead>";

echo "<tbody>";
    //表にフェッチした総数だけ記事を表示
    while($rss_list_count <= $rss_list_all - 1) {
         //print_r($tbl);
         $link_title = mb_convert_encoding($rss_list_array[$rss_list_count][0], "UTF-8");//UTF-8にエンコード(文字化け防止)

        echo "<tr>";
            //ブログタイトル
            echo "<td class='four'><a href=./blog?url=".$rss_list_array[$rss_list_count][1].">".$link_title."</a></td>";
            //ブログURL
            echo "<td class='three'><a href=".$rss_list_array[$rss_list_count][1]." target=_blank>".$rss_list_array[$rss_list_count][1]."</a></td>";
            //カテゴリタグ1
            echo "<td class='two'><a href=#>".$rss_list_array[$rss_list_count][2]."</a></td>";
            //カテゴリタグ2
            echo "<td class='two'><a href=# >".$rss_list_array[$rss_list_count][3]."</a></td>";
            //RSS登録更新日
            echo "<td class='three'>".$rss_list_array[$rss_list_count][4]."</a></td>";
            //最新記事更新日
            echo "<td class='three'>".$rss_list_array[$rss_list_count][5]."</a></td>";
        echo "</tr>";

        $rss_list_count++;
    }
echo "</tbody>";
?>

<tfoot>
<tr><th><?php echo "登録RSS数：".($rss_list_all - 1); ?></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
</tr></tfoot>
