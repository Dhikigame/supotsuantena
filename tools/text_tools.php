<?php
//マルチバイト対応のwordwrap関数
function mb_wordwrap( $str, $width, $break ){
    $c = mb_strlen($str);
    $arr = [];
    for ($i=0; $i<=$c; $i+=$width) {
        $arr[] = mb_substr($str, $i, $width);
    }
    return implode($break, $arr);
}

//特定の英字続きをエスケープ処理する(サイトレイアウト崩れ防止)
function title_escape($article_title){

    if(strpos($article_title,'wwiwwi') !== false){
        $article_title = str_replace("wwiwwi", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'wwemme') !== false){
        $article_title = str_replace("wwemme", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'wywy') !== false || strpos($article_title,'ｗｙｗｙ') !== false){
        $article_title = str_replace("wywy", "", $article_title);
        $article_title = str_replace("ｗｙｗｙ", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'wwxwwx') !== false){
        $article_title = str_replace("wwxwwx", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'wwywwy') !== false){
        $article_title = str_replace("wwywwy", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'wxwx') !== false || strpos($article_title,'ｗｘｗｘ') !== false){
        $article_title = str_replace("wxwx", "", $article_title);
        $article_title = str_replace("ｗｘｗｘ", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'ｗｗｗ') !== false || strpos($article_title,'wwwww') !== false){
        $article_title = str_replace("ｗｗｗ", "", $article_title);
        $article_title = str_replace("wwwww", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'ωωω') !== false){
        $article_title = str_replace("ωωω", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'ｗａｗａ') !== false){
        $article_title = str_replace("ｗａｗａ", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'wmnwmn') !== false){
        $article_title = str_replace("wmnwmn", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'！！！！！') !== false){
        $article_title = str_replace("！！！！！", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'ｗｗ。') !== false){
        $article_title = str_replace("ｗｗ。", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'wwwxwwwx') !== false || strpos($article_title,'ｗｗｗｘ') !== false){
        $article_title = str_replace("wwwxwwwx", "", $article_title);
        $article_title = str_replace("ｗｗｗｘ", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'wewe') !== false){
        $article_title = str_replace("wewe", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'xxxxx') !== false){
        $article_title = str_replace("xxxxx", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'wwwt') !== false){
        $article_title = str_replace("wwwt", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'yyyywywyywyw') !== false){
        $article_title = str_replace("yyyywywyywyw", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'ｗｗｘｘ') !== false){
        $article_title = str_replace("ｗｗｘｘ", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'wwewwe') !== false){
        $article_title = str_replace("wwewwe", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'yyyyy') !== false){
        $article_title = str_replace("yyyyy", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'wwuwwu') !== false){
        $article_title = str_replace("wwuwwu", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'wowo') !== false){
        $article_title = str_replace("wowo", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'ｗｗｙ') !== false){
        $article_title = str_replace("ｗｗｙ", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'aaaaa') !== false){
        $article_title = str_replace("aaaaa", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'uuuuu') !== false){
        $article_title = str_replace("uuuuu", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'wnwn') !== false){
        $article_title = str_replace("wnwn", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'шшш') !== false){
        $article_title = str_replace("шшш", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'wwwz') !== false){
        $article_title = str_replace("wwwz", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'w@w@') !== false){
        $article_title = str_replace("w@w@", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'uwv') !== false){
        $article_title = str_replace("uwv", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'wwrwwr') !== false){
        $article_title = str_replace("wwrwwr", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'wwwywwwy') !== false || strpos($article_title,'ｗｗｗｙ') !== false){
        $article_title = str_replace("wwwywwwy", "", $article_title);
        $article_title = str_replace("ｗｗｗｙ", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'wwwhwwwh') !== false || strpos($article_title,'ｗｗｗｈ') !== false){
        $article_title = str_replace("wwwhwwwh", "", $article_title);
        $article_title = str_replace("ｗｗｗｈ", "", $article_title);
        return $article_title;
    }
    if(strpos($article_title,'wwwjwwwj') !== false || strpos($article_title,'ｗｗｗｊ') !== false){
        $article_title = str_replace("wwwjwwwj", "", $article_title);
        $article_title = str_replace("ｗｗｗｊ", "", $article_title);
        return $article_title;
    }

    return $article_title;
}