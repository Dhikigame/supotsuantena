<?php
/* ドメイン疎通確認プログラム*/
function url_domain_analysis($url){
    $url = parse_url($url);

    // ドメインを取りたい場合
    $hostname = $url['host'];
    echo $hostname."<br>";
    //ドメインからIPアドレス取得
    $ip = gethostbyname($hostname);
    echo $ip."<br>";
    //IPアドレスの疎通を確認
    $up = ping($ip);

    return $up ? 1 : 0;

}

/* Ping送信プログラム */
function ping($host){
    $r = exec(sprintf('ping -c 1 -W 5 %s', escapeshellarg($host)), $res, $rval);
    //pingの結果を表示
    //print_r($r);
    return $rval === 0;
}
?>