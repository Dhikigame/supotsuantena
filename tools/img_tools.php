<?php
/*
//画像が出力できるか判定　画像ない場合は「No Image」出力
function img_judge($img, $ping){

    //ブログとのネットワークの疎通が取れなければ「No Image」を出力
    if($ping === 0){
        return 'img/noimage.png';
    }

    $extension_list = array(".jpg", ".jpeg", ".JPG", ".JPEG", ".jpe", ".jfif", ".pjpeg", ".pjp", ".png", ".gif");//画像拡張子

    foreach ($extension_list as $extension){
        //当確の拡張子があればそのままブラウザに出力
        if (strpos($img, $extension) !== false){
            return $img;
        }
    }
    $ua = $_SERVER['HTTP_USER_AGENT'];

    if((strpos($ua,'iPhone')!==false) || (strpos($ua,'iPod')!==false) || (strpos($ua,'Android.*Mobile')!==false) || (strpos($ua,'Windows.*Phone')!==false) || (strpos($ua,'Android')!==false)) {
        return '../img/noimage.png';
    }else{
        return 'img/noimage.png';
    }

}
*/

//画像が出力できるか判定　画像ない場合は「No Image」出力
function img_judge($img){

    $extension_list = array(".jpg", ".jpeg", ".JPG", ".JPEG", ".jpe", ".jfif", ".pjpeg", ".pjp", ".png", ".gif");//画像拡張子

    foreach ($extension_list as $extension){
        //当確の拡張子があればそのままブラウザに出力
        if (strpos($img, $extension) !== false){
            return $img;
        }
    }
    $ua = $_SERVER['HTTP_USER_AGENT'];

    if((strpos($ua,'iPhone')!==false) || (strpos($ua,'iPod')!==false) || (strpos($ua,'Android.*Mobile')!==false) || (strpos($ua,'Windows.*Phone')!==false) || (strpos($ua,'Android')!==false)) {
        return '../img/noimage.png';
    }else{
        return 'img/noimage.png';
    }

}


//画像のサイズを変更
function img_size($img){
    
    if($img == 'img/noimage.png' || $img == '../img/noimage.png'){
        return "width='225' height='187'";
    }
    
    //画像のサイズ情報を取得
    $image = getimagesize($img);

    if($image){
        //width : height = x : 187 → x * height = 187 * width → x = 187 * width / height
        $width = floor(187 * $image[0] / $image[1]);//heightのサイズを187pxに固定し、それに合わせた比率のwidthのサイズを設定
        //widthのサイズが332pxを超えていたら332pxに固定
        if($width > 332){
            $width = 332;
        }
    }else{
        return "width='332' height='187'";
    }

    return "width='".$width."' height='187'";
}


?>