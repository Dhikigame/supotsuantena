<?php
print<<<eof
  <header class="header">
    <div class="ui menu">
      <a class="item" href=./baseball>
        野球<i class="baseball ball icon"></i>
      </a>
      <a class="item" href=./soccer>
        サッカー<i class="center futbol outline icon"></i>
      </a>
      <a class="item" href=./basketball>
        バスケ<i class="basketball ball icon"></i>
      </a>              
      <a class="item" href=./information>
        <i class="info circle icon"></i>Information
      </a>
      <a class="item right aligned title" id="title" href=./index>
        <img src="img/sport_antena.png">
      </a>
    </div>
  </header>
eof;
?>