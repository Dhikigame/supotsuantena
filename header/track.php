<?php
print<<<eof
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-85902327-3"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-85902327-3');
        </script>
eof;
?>