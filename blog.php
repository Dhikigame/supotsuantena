<?php

require_once './php_firstdown/php_firstDB.php';//DBサーバーに接続
require_once './tools/text_tools.php';//文字列編集
require_once './db/db_timeupdate.php';//RSS更新時間取得
require_once 'time_display.php';//RSS更新時間表示
require_once './db/db_article.php';//ブログのURLからDBの記事取り出す
require_once './db/db_select_category.php';//ブログにあらかじめ登録されたタグを取得する(1〜2個)
require_once 'tag/tag_display.php';//ブラウザに野球関連タグを表示

$ua = $_SERVER['HTTP_USER_AGENT'];
//echo $ua;
if((strpos($ua,'iPhone')!==false) || (strpos($ua,'iPod')!==false) || (strpos($ua,'Android.*Mobile')!==false) || (strpos($ua,'Windows.*Phone')!==false) || (strpos($ua,'Android')!==false)) {
  header('Location:/sp/');
  exit();
}
?>
<?php
$url = isset($_GET['url']) ? $_GET['url'] : null; //GETでIDを受け取る
if($url == null){
  $result_article = article_query($link);//SQLの結果を出力
  $title = "スポーツアンテナ！";
}else{
  $blog_array = article_query_blog($link, $url);//SQLの結果を出力
  $result_article = $blog_array[0];
  $mainmatome_name = $blog_array[1];
  $title = "スポーツアンテナ！ - ブログ: ".$mainmatome_name;
}
?>
<!DOCTYPE html>
<html lang="en">
<meta name="description" content="スポーツ関連のまとめブログや個人ブログをカテゴリごとに検索できるアンテナサイトです。" />
  <head>
    <title><?php echo $title; ?></title>      
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- ページのレイアウト 読み込み --> 
    <link rel="stylesheet" type="text/css" href="css/layout.css">    
    <!-- jquery 読み込み -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <!-- semantic.js 読み込み -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.6/semantic.min.js" integrity="sha256-HcON65l89nxNl7vaRECVPe/zNDBzhCcJYBE7O+EcruU=" crossorigin="anonymous"></script>
    <!-- semantic.css 読み込み --> 
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.3/semantic.min.css">
    <!-- スクロール関連スクリプト -->
    <script src="./js/scroll.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <?php require_once './header/track.php'; ?>
  </head>

  <body ontouchstart="">
    <?php require_once 'header/header.php'; ?>
    <img src='img/pagetop.png' id='top' width='30' height='100'>

    <div class="time" style="text-align:right;">
      <a href src='./blog'><i class='big redo icon' id='redo'></i></a>
      <?php time_display(timeupdate($link));//更新時刻取得 ?>
    </div>

    <div class=" ui center aligned container">
      <table class="ui celled table">
        <tbody>

<?php
    /*  ブログのタイトルがある場合は表示  */
    if($mainmatome_name != null){
      echo "<div class='searchbox'>";
          echo "<div class='searchbox-title'>検索ブログ</div>";
          echo "<p>".$mainmatome_name."</p>";
      echo "</div>";
    }
?>

<?php

require_once './article_display.php';//ブラウザに記事一覧表示

?>

        </tbody>
      </table>
    </div>
  </body>
</html>

<?php
require_once './php_firstdown/php_downDB.php';//DB接続を終了
?>