# スポーツアンテナ！

スポーツ関連の5ch系まとめブログや個人ブログをまとめたアンテナサイトです。
ブログは各スポーツカテゴリごとにまとめられます。また、記事の「タイトル」「subject」を解析し関連したタグが登録されます。
これにより「チーム」「リーグ」ごとで検索すれば関連した記事が一覧で見られるようになっています。

It is an antenna site that summarizes sports-related 5ch summary blogs and personal blogs.
Blogs are organized by sports category. In addition, the "title" and "subject" of the article are analyzed and related tags are registered.
As a result, if you search by "team" or "league", you can see related articles in a list.

# Release

http://www.sport-antena.com



# Requirement

- PHP7

- MySQL

- Javascript

- HTML5

- Semantic UI

  

  Operating environment

  * さくらVPS

  * CentOS7 × 2(Web+API Server、DB Server) 

  

# Features

* 1分ごとに記事が更新されます。尚、定期的にメンテナンスが入り記事が更新されない期間がございますがご了承ください。
* タグをクリック・タップすることでタグに関連した記事が検索されます。
* 記事タイトルの下に「タイトル」「subject」を解析した結果のタグが表示されます。
* ブログタイトルの下に登録されているタグが表示されます。

# Author

- Dhiki(Infrastructure engineer & Video contributor)
- https://twitter.com/DHIKI_pico