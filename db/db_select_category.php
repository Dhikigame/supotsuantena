<?php

require_once 'sql_sec.php';//SQL関連のセキュリティ
/***
		カテゴリテーブルデータ取得用プログラム
***/
function category_tag_select($mainmatome_URL, $link){
 
    $db = DB_select_category($link);
    $mainmatome_URL = sql_injection($link, $mainmatome_URL);//SQLインジェクション防止

    $select_category_tag1 = "SELECT category_sport FROM CATEGORY_SPORTS WHERE mainmatome_URL = '".$mainmatome_URL."'";
    $select_category_tag2 = "SELECT category_sport2 FROM CATEGORY_SPORTS WHERE mainmatome_URL = '".$mainmatome_URL."'";

    $query1 = mysqli_query($link, $select_category_tag1);//SQLのクエリ送信（クエリ：DBに情報要求）
    //クエリ取得できないならエラー
        if (!$query1){
            die("サイトが動いてません！");
        }
    $fetch_tag1 = mysqli_fetch_array($query1);

    $query2 = mysqli_query($link, $select_category_tag2);//SQLのクエリ送信（クエリ：DBに情報要求）
    //クエリ取得できないならエラー
        if (!$query2){
            die("サイトが動いてません！");
        }
    $fetch_tag2 = mysqli_fetch_array($query2);

    $tag[0] = $fetch_tag1[0];
    $tag[1] = $fetch_tag2[0];

    return $tag;
}
?>