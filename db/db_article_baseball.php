<?php

//DBにある野球タグのカラムを全取得し、SQLの結果を出力
function article_query_baseball($link){
    //野球タグの入ったブログのテーブル名全取得
    $db = DB_select_category($link);
    $baseball_tbl = "select table_name from CATEGORY_SPORTS where category_sport = '野球' OR category_sport2 = '野球'";

    $result_tbl= mysqli_query($link, $baseball_tbl);//SQLのクエリ送信（クエリ：DBに情報要求）
        if (!$result_tbl){//クエリ取得できないならエラー
            die("エラー：サイトが動いていません！");
        }
    $rows_tbl = mysqli_num_rows($result_tbl);//SQLの結果の行数を取得
    //echo $result_tbl;
    $db = DB_select($link);//MATOME_ANTENNAデータベースに切り替え
    //MATOME_ANTENNAデータベースから野球の記事取得
    if($rows_tbl){//SQLの結果あるなら出力
        $i = 0;
            while($tbl = mysqli_fetch_array($result_tbl)) {
                //echo $tbl[0]."<br>";
                //$tbl_name[$i] = $tbl[0];
                if($i >= 1){
                    $baseball_article = $baseball_article." UNION SELECT article_TITLE, article_URL, article_IMG, create_DATE, mainmatome_URL, mainmatome_name, main_tag, sub_tag1, sub_tag2, sub_tag3, sub_tag4, sub_tag5, sub_tag6, sub_tag7, sub_tag8, sub_tag9, sub_tag10, sub_tag11, sub_tag12 FROM ".$tbl[0]." WHERE create_DATE >= DATE_ADD(NOW(), INTERVAL -3 DAY)";
                    $i++;
                }
                if($i == $rows_tbl){
                    $baseball_article = $baseball_article." ORDER BY create_DATE DESC";
                }
                if($i == 0){
                    $baseball_article = "SELECT article_TITLE, article_URL, article_IMG, create_DATE, mainmatome_URL, mainmatome_name, main_tag, sub_tag1, sub_tag2, sub_tag3, sub_tag4, sub_tag5, sub_tag6, sub_tag7, sub_tag8, sub_tag9, sub_tag10, sub_tag11, sub_tag12 FROM ".$tbl[0]." WHERE create_DATE >= DATE_ADD(NOW(), INTERVAL -3 DAY)";    
                    $i++;
                }
                //echo $tbl[0];
            }
    }
    $result_article = mysqli_query($link, $baseball_article);//SQLのクエリ送信（クエリ：DBに情報要求）
    //echo $baseball_article;
    if (!$result_article){//クエリ取得できないならエラー
        die("エラー：サイトが動いていません！");
    }

    //SQLの結果あるなら出力
    return $result_article;
}