<?php

//DBにある野球タグのカラムを全取得し、SQLの結果を出力
function article_query_soccer($link){
    //野球タグの入ったブログのテーブル名全取得
    $db = DB_select_category($link);
    $soccer_article = "select table_name from CATEGORY_SPORTS where category_sport = 'サッカー' OR category_sport2 = 'サッカー'";

    $result_tbl= mysqli_query($link, $soccer_article);//SQLのクエリ送信（クエリ：DBに情報要求）
        if (!$result_tbl){//クエリ取得できないならエラー
            die("エラー：サイトが動いていません！");
        }
    $rows_tbl = mysqli_num_rows($result_tbl);//SQLの結果の行数を取得
    //echo $result_tbl;
    $db = DB_select($link);//MATOME_ANTENNAデータベースに切り替え
    //MATOME_ANTENNAデータベースから野球の記事取得
    if($rows_tbl){//SQLの結果あるなら出力
        $i = 0;
            while($tbl = mysqli_fetch_array($result_tbl)) {
                //echo $tbl[0]."<br>";
                //$tbl_name[$i] = $tbl[0];
                if($i >= 1){
                    $soccer_article = $soccer_article." UNION SELECT article_TITLE, article_URL, article_IMG, create_DATE, mainmatome_URL, mainmatome_name, main_tag, sub_tag1, sub_tag2, sub_tag3, sub_tag4, sub_tag5, sub_tag6, sub_tag7, sub_tag8, sub_tag9, sub_tag10, sub_tag11, sub_tag12 FROM ".$tbl[0]." WHERE create_DATE >= DATE_ADD(NOW(), INTERVAL -3 DAY)";
                    $i++;
                }
                //野球関係のブログ記事にサッカータグが含まれているか検索、含まれているなら記事のカラム取得
                if($i == $rows_tbl){
                    $db = DB_select_category($link);
                    $baseball_article = "select table_name from CATEGORY_SPORTS where category_sport = '野球' OR category_sport2 = '野球'";

                    $result_tbl= mysqli_query($link, $baseball_article);//SQLのクエリ送信（クエリ：DBに情報要求）
                    if (!$result_tbl){//クエリ取得できないならエラー
                        die("エラー：サイトが動いていません！");
                    }
                    $rows_baseball_tbl = mysqli_num_rows($result_tbl);//SQLの結果の行数を取得
                    //echo $result_tbl;
                    $db = DB_select($link);//MATOME_ANTENNAデータベースに切り替え
                    //MATOME_ANTENNAデータベースから野球の記事取得
                    if($rows_baseball_tbl){//SQLの結果あるなら出力
                        $j = 0;
                            while($tbl = mysqli_fetch_array($result_tbl)) {
                                //echo $tbl[0]."<br>";
                                //$tbl_name[$i] = $tbl[0];
                                if($j >= 0){
                                    $soccer_article = $soccer_article." UNION SELECT article_TITLE, article_URL, article_IMG, create_DATE, mainmatome_URL, mainmatome_name, main_tag, sub_tag1, sub_tag2, sub_tag3, sub_tag4, sub_tag5, sub_tag6, sub_tag7, sub_tag8, sub_tag9, sub_tag10, sub_tag11, sub_tag12 FROM ".$tbl[0];
                                    $soccer_article = $soccer_article." WHERE sub_tag1 = 'サッカー' OR sub_tag1 = '海外サッカー' OR sub_tag1 = '国内サッカー' OR sub_tag2 = 'サッカー' OR sub_tag2 = '海外サッカー' OR sub_tag2 = '国内サッカー' OR sub_tag3 = 'サッカー' OR sub_tag3 = '海外サッカー' OR sub_tag3 = '国内サッカー' OR";
                                    $soccer_article = $soccer_article." sub_tag4 = 'サッカー' OR sub_tag4 = '海外サッカー' OR sub_tag4 = '国内サッカー' OR sub_tag5 = 'サッカー' OR sub_tag5 = '海外サッカー' OR sub_tag5 = '国内サッカー' OR sub_tag6 = 'サッカー' OR sub_tag6 = '海外サッカー' OR sub_tag6 = '国内サッカー' OR";
                                    $soccer_article = $soccer_article." sub_tag7 = 'サッカー' OR sub_tag7 = '海外サッカー' OR sub_tag7 = '国内サッカー' OR sub_tag8 = 'サッカー' OR sub_tag8 = '海外サッカー' OR sub_tag8 = '国内サッカー' OR sub_tag9 = 'サッカー' OR sub_tag9 = '海外サッカー' OR sub_tag9 = '国内サッカー' OR";
                                    $soccer_article = $soccer_article." sub_tag10 = 'サッカー' OR sub_tag10 = '海外サッカー' OR sub_tag10 = '国内サッカー' OR sub_tag11 = 'サッカー' OR sub_tag11 = '海外サッカー' OR sub_tag11 = '国内サッカー' OR sub_tag12 = 'サッカー' OR sub_tag12 = '海外サッカー' OR sub_tag12 = '国内サッカー'";
                                    $j++;
                                }
                                if($j == $rows_baseball_tbl){
                                    $soccer_article = $soccer_article." ORDER BY create_DATE DESC";
                                }
                                //echo $tbl[0];
                            }
                    }
                }
                if($i == 0){
                    $soccer_article = "SELECT article_TITLE, article_URL, article_IMG, create_DATE, mainmatome_URL, mainmatome_name, main_tag, sub_tag1, sub_tag2, sub_tag3, sub_tag4, sub_tag5, sub_tag6, sub_tag7, sub_tag8, sub_tag9, sub_tag10, sub_tag11, sub_tag12 FROM ".$tbl[0];    
                    $i++;
                }
                //echo $tbl[0];
            }
    }
    $result_article = mysqli_query($link, $soccer_article);//SQLのクエリ送信（クエリ：DBに情報要求）
    //echo $soccer_article;
    if (!$result_article){//クエリ取得できないならエラー
        die("エラー：サイトが動いていません！");
    }

    //SQLの結果あるなら出力
    return $result_article;
}