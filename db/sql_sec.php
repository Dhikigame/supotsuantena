<?php
/***
    SQL関連のセキュリティ処理を行う
***/

//ディレクトリトラバーサル攻撃防止、XSS防止
function directory_XSS_sec($tag){
    if (preg_match('/(\.\.\/|\/|\.\.\\\\)/', $tag)) {
        echo "無効なタグです";
    }else{
        return htmlspecialchars($tag, ENT_QUOTES, 'UTF-8');
    }
}

//SQLインジェクション防止(エスケープ処理をする)
function sql_injection($link, $sql){
    return mysqli_real_escape_string($link, $sql);
}
?>