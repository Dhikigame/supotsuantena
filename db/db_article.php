<?php

//DBにあるテーブルに登録されている記事のカラムを全取得し、SQLの結果を出力
function article_query($link){
    //テーブル全取得
    $db = DB_select($link);
    $all_tbl = "SHOW TABLES FROM MATOME_ANTENNA";

    $result_tbl= mysqli_query($link, $all_tbl);//SQLのクエリ送信（クエリ：DBに情報要求）
        if (!$result_tbl){//クエリ取得できないならエラー
            die("エラー：サイトが動いていません！");
        }
    $rows_tbl = mysqli_num_rows($result_tbl);//SQLの結果の行数を取得

    if($rows_tbl){//SQLの結果あるなら出力
        $i = 0;
          while($tbl = mysqli_fetch_array($result_tbl)) {
            //echo $tbl[0]."<br>";
            if($tbl[0] != 'CATEGORY_SPORTS'){
                $tbl_name[$i] = $tbl[0];
                $i++;
            }
          }
          $j = $i;
    }

    //テーブルからカラム取得
    $i = 0;
    $all_article = "SELECT article_TITLE, article_URL, article_IMG, create_DATE, mainmatome_URL, mainmatome_name, main_tag, sub_tag1, sub_tag2, sub_tag3, sub_tag4, sub_tag5, sub_tag6, sub_tag7, sub_tag8, sub_tag9, sub_tag10, sub_tag11, sub_tag12 FROM ".$tbl_name[$i]." WHERE create_DATE >= DATE_ADD(NOW(), INTERVAL -3 DAY)";
    while($j > $i){
      $i++;
      if($j == $i){
        $all_article = $all_article." ORDER BY create_DATE DESC";
        break;
      }
      $all_article = $all_article." UNION SELECT article_TITLE, article_URL, article_IMG, create_DATE, mainmatome_URL, mainmatome_name, main_tag, sub_tag1, sub_tag2, sub_tag3, sub_tag4, sub_tag5, sub_tag6, sub_tag7, sub_tag8, sub_tag9, sub_tag10, sub_tag11, sub_tag12 FROM ".$tbl_name[$i]." WHERE create_DATE >= DATE_ADD(NOW(), INTERVAL -3 DAY)";
    }

    $result_article = mysqli_query($link, $all_article);//SQLのクエリ送信（クエリ：DBに情報要求）
    if (!$result_article){//クエリ取得できないならエラー
        die("エラー：サイトが動いていません！");
    }

    //SQLの結果あるなら出力
    return $result_article;
}


//DBにあるテーブルのカラムを全取得し、SQLの結果を出力
function rss_list_query($link){
  //テーブル全取得
  $db = DB_select_category($link);

  //テーブルからカラム取得
  $i = 0;
  $all_rss_list = "SELECT mainmatome_name, mainmatome_URL, category_sport, category_sport2, rss_DATE, regist_DATE FROM CATEGORY_SPORTS";
  $all_rss_list .= " ORDER BY regist_DATE DESC";

  $result_rss_list[0] = mysqli_query($link, $all_rss_list);//SQLのクエリ送信（クエリ：DBに情報要求）

  if (!$result_rss_list[0]){//クエリ取得できないならエラー
      die("エラー：サイトが動いていません！");
  }
  $result_rss_list[1] = mysqli_num_rows($result_rss_list[0]);
  //SQLの結果あるなら出力
  return $result_rss_list;
}

//DBにあるブログのURLのカラムを全取得し、SQLの結果を出力
function article_query_blog($link, $url){
  /*  ブログのテーブルを取得  */
  DB_select_category($link);

  $tbl_name = "select table_name FROM CATEGORY_SPORTS where mainmatome_URL = '".$url."'";

  $result_tbl= mysqli_query($link, $tbl_name);//SQLのクエリ送信（クエリ：DBに情報要求）
  if (!$result_tbl){//クエリ取得できないならエラー
      die("エラー：サイトが動いていません！");
  }
  $rows_tbl = mysqli_num_rows($result_tbl);//SQLの結果の行数を取得

  if($rows_tbl){//SQLの結果あるなら出力
    $tbl = mysqli_fetch_array($result_tbl);
    $tbl_name = $tbl[0];
  }
  /*  ブログのタイトルを取得  */
  $mainmatome_name = "select mainmatome_name FROM CATEGORY_SPORTS where mainmatome_URL = '".$url."'";

  $result_tbl= mysqli_query($link, $mainmatome_name);//SQLのクエリ送信（クエリ：DBに情報要求）
  if (!$result_tbl){//クエリ取得できないならエラー
      die("エラー：サイトが動いていません！");
  }
  $rows_tbl = mysqli_num_rows($result_tbl);//SQLの結果の行数を取得

  if($rows_tbl){//SQLの結果あるなら出力
    $tbl = mysqli_fetch_array($result_tbl);
    $mainmatome_name = $tbl[0];
  }

    /*  テーブルからカラム取得  */
    DB_select($link);
    $all_article = "SELECT article_TITLE, article_URL, article_IMG, create_DATE, mainmatome_URL, mainmatome_name, main_tag, sub_tag1, sub_tag2, sub_tag3, sub_tag4, sub_tag5, sub_tag6, sub_tag7, sub_tag8, sub_tag9, sub_tag10, sub_tag11, sub_tag12  FROM ".$tbl_name;
    $all_article .= " ORDER BY create_DATE DESC";

    $result_article = mysqli_query($link, $all_article);//SQLのクエリ送信（クエリ：DBに情報要求）
    if (!$result_article){//クエリ取得できないならエラー
        die("エラー：サイトが動いていません！");
    }

    $blog_array[0] = $result_article;
    $blog_array[1] = $mainmatome_name;

    //SQLの結果あるなら出力
    return $blog_array;
}